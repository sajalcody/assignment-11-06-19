import java.util.*;

public class Ranking {
	public static void main(String[] args) {
		File scoreFile = new File(/home/pallavi);

		BufferedReader br = new BufferedReader(new FileReader(scoreFile));
		TreeMap<Integer, String> rank = new TreeMap<Integer, String>(Collections.reverseOrder());

		String student;
		while ((student = br.readLine()) != null) {
			String[] details = student.split(" ");

			rank.put((int)details[1], details[0]);
		}
		Set set = rank.entrySet();
		Iterator i = set.iterator();
		prevRank = 0
		rankGiven = 1

		System.out.println("Rank\tRoll_No\tMarks");
		while(i.hasNext()) {
			if(prevRank == rankGiven)
				System.out.print(prevRank);

			else
				System.out.print(rankGiven);

			Map.Entry thisRank = (Map.Entry)i.next();
			System.out.print(thisRank.getKey() + "\t");
			System.out.print(thisRank.getValue());
			prevRank = rankGiven;
			rankGiven++;
		}
	}
}
