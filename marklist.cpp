#include <bits/stdc++.h>
using namespace std;

map<int,vector<string>> getMarks(string file_name){
    map<int,vector<string>> marks2RollNumber;
    ifstream file (file_name);
    string word;
    while ( file >> word )
    {
      string rollNumber = word;
      file >> word;
      int marks = stoi(word);
      if (marks2RollNumber.find(marks) == marks2RollNumber.end()){
          vector <string> rollNumbers;
          rollNumbers.push_back(rollNumber);
          marks2RollNumber[marks] = rollNumbers;
      }
      else
      {
           marks2RollNumber[marks].push_back(rollNumber);
      }      
    }
    file.close();
    return marks2RollNumber;
}

int main(int argc, char *argv[]){
   map<int,vector<string>> marks =  getMarks("../resources/marklist.txt");
   map<int,vector<string>> :: iterator it;
   vector<string> roll;
   vector<string> :: iterator vit;
   for (it = marks.begin(); it != marks.end(); it++){
       cout<<(*it).first;
       roll = (*it).second;
        for (vit = roll.begin();vit != roll.end(); it++)
            cout<<*vit;
   }
    return 0;
}