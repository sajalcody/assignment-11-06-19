import sys

def minReading(size):
    return ''.join([str(d) for d in range(1,size+1)])

def maxReading(size):
    return ''.join([str(d) for d in range(10-size,10)])

def isAscending(reading: str) -> bool:
    for i in range(1, len(reading)):
        if reading[i] <= reading[i-1]:
            return False
    return True

def findNext(reading):
    if reading == maxReading(len(reading)):
        return minReading(len(reading))
    reading = str(int(reading) + 1)
    if isAscending(reading):
        return reading
    return findNext(reading)

def findNextN(reading, n):
    nReadings = ""
    while n > 0:
        reading = findNext(reading)
        nReadings += reading + ","
        n -=1
    return nReadings

def findPrevious(reading):
    if reading == minReading(len(reading)):
        return maxReading(len(reading))
    reading = str(int(reading) - 1)
    if isAscending(reading):
        return reading
    return findPrevious(reading)

def findPreviousN(reading, n):
    nReadings = ""
    while n > 0:
        reading = findPrevious(reading)
        nReadings += reading + ","
        n -=1
    return nReadings

def findDifference(start,finish):
    diff = 0
    if start <= finish: 
        while(start != finish):
            start = findNext(start)
            diff += 1
    else:
        while(start != finish):
            start = findPrevious(start)
            diff += 1
    return diff

size = int(sys.argv[1])
print(minReading(size),maxReading(size))
print(findNextN("234",8))
print(findPrevious("239",8))


